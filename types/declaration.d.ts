declare module 'bedrock-protocol' {
    export interface Options {
        profilesFolder: string,
    }

    export interface Client {
        username: string;
        profile: {
            name: string;
            xuid: string;
        }

        on(event: string, handler: (...args: any[]) => void);
        on(event: 'text', handler: (packet: Packet) => void);
    }

    export const enum TranslationMessage {
        USER_JOINED = '§e%multiplayer.player.joined',
        USER_LEFT = '§e%multiplayer.player.left'
    }

    export type Packet = {
        type: 'chat';
        needs_translation: boolean;
        source_name: string;
        message: string;
        xuid: string;
    } | {
        type: 'translation';
        needs_translation: true;
        message: TranslationMessage.USER_JOINED|TranslationMessage.USER_LEFT;
        parameters: [string];
    } | {
        type: 'translation';
        needs_translation: true;
        message: string;
        parameters: string[];
    } | ({ type: string } & any)
}
