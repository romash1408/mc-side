ARG NODE_VERSION=16.13.0


FROM node:$NODE_VERSION-alpine as build


WORKDIR /app

COPY package.json yarn.lock ./
RUN yarn
COPY raknet-prebuild-linux-4-x64.node node_modules/raknet-native/prebuilds/linux-4-x64/node-raknet.node

COPY . .

CMD ["yarn", "start"]
