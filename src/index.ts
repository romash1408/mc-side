require('dotenv').config({ path: '.env' });

import { createClient, TranslationMessage } from 'bedrock-protocol';
import DiscordClient, { Color } from './DiscordClient';
import i18n from './i18n';
import logger from './logger';

const log = logger();

const client = createClient({
    host: process.env.BEDROCK_HOST,
    port: Number(process.env.BEDROCK_PORT),
    offline: false,
    profilesFolder: '.profiles',
});

const discord = new DiscordClient({
    token: process.env.DISCORD_TOKEN,
    channelId: process.env.DISCORD_CHANNEL,
});

const gameTranslations = i18n.getCatalog('en')['message']['game-translation'];

client.on('text', (packet) => {
    log.debug(i18n.__('logs.bedrock.got-packet'), packet);
    switch(packet.type) {
        case 'chat':
            discord.write({
                text: `[**${packet.source_name}**] ${packet.message}`,
            });
            break;
        case 'translation':
            const key = packet.message.replace(/\./g, '-');
            if (key in gameTranslations) {
                let color = Color.BLUE;
                switch (packet.message) {
                    case TranslationMessage.USER_JOINED: color = Color.GREEN; break;
                    case TranslationMessage.USER_LEFT: color = Color.RED; break;
                }
                try {
                    discord.write({
                        text: i18n.__(`message.game-translation.${key}`, ...packet.parameters),
                        color,
                    });
                } catch (e) {
                    log.error(e);
                }
                break;
            }
            log.warn(i18n.__('logs.bedrock.unknown-translation-message', key), packet.parameters);
            break;
    }
    // client.queue('text', {
    //     type: 'chat', needs_translation: false, source_name: client.username, xuid: '', platform_chat_id: '',
    //     message: `${packet.source_name} said: ${packet.message} on ${new Date().toLocaleString()}`
    // })
});

client.on('join', () => log.info(i18n.__('logs.bedrock.joined', client.username, client.profile.xuid)));
client.on('disconnect', (params: { hide_disconnect_reason: boolean, message: string }) => {
    params.hide_disconnect_reason = true;
    log.error(params.message);
    process.exit(1);
});

let stopped = false;
['exit', 'SIGINT', 'SIGUSR1', 'SIGUSR2', 'uncaughtException', 'SIGTERM'].forEach((eventType) => {
    process.on(eventType, (...args) => {
        if (stopped) {
            return;
        }
        log.info(i18n.__('logs.system.stop-signal', eventType));
        stopped = true;
        log.info(i18n.__('logs.system.stop-process'));
        client.close();
        discord.close();
        log.info(i18n.__('logs.system.connection-closed'));
        if (eventType === 'uncaughtException') {
            log.error(args[0]);
            process.exit(1);
        }
    });
})
