import consola = require('consola/dist/consola');
import { Consola } from 'consola';
const caller = require('caller');

export default function logger (): Consola {
    const callerFile = caller().split(/\/src\//)[1];
    return consola.withTag(callerFile);
}
