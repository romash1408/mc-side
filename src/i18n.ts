import * as fs from 'fs';
import * as yaml from 'js-yaml';
import { I18n, configure } from 'i18n';

function load(lang: string): any {
    const path = require('path').join(__dirname, 'locales', `${lang}.yaml`);
    const file = fs.readFileSync(path, 'utf8');
    return yaml.load(file, {
        filename: path,
    });
}

const langs = [
    'en',
    'ru',
];

const i18n = {} as I18n;
configure({
    register: i18n,
    staticCatalog: Object.fromEntries(langs.map(lang => [lang, load(lang)])),
    fallbacks: Object.fromEntries(langs.map(lang => [lang + '-*', lang])),
    objectNotation: true,
});
i18n.setLocale(Intl.DateTimeFormat().resolvedOptions().locale);

export default i18n;
